package com.seren.brn3.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by Herold on 27.06.2016.
 */
public class TodoMVCPage {
    ElementsCollection tasks = $$("#todo-list>li");
    SelenideElement newTodo = $("#new-todo");


    @Step
    public void add(String... taskTexts) {
        for(String text : taskTexts){
            newTodo.setValue(text).pressEnter();
        }
    }

    @Step
    public void assertTasks(String... taskTexts){
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertVisibleTasks(String... taskTexts){
        tasks.filterBy(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertNoVisibleTasks(){
        tasks.filterBy(visible).shouldBe(empty);
    }

    @Step
    public void assertItemsLeft(int tasksLeft){
        $("#todo-count>strong").shouldBe(exactText(Integer.toString(tasksLeft)));
    }

    @Step
    public void delete(String taskText){
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public void toggle(String taskText){
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public void clearCompleted(){
        $("#clear-completed").click();
    }

    @Step
    public void toggleAll(){
        $("#toggle-all").click();
    }

    @Step
    public void assertNoTasks(){
        tasks.shouldBe(empty);
    }

    @Step
    public void edit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public void cancelEdit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public void confirmEditByPressTab(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    @Step
    public void confirmEditByClickOutside(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    @Step
    public void deleteByEmptyingText(String TaskText){
        startEdit(TaskText, "").pressEnter();
    }

    @Step
    public SelenideElement startEdit (String oldTaskText, String newTaskText){
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public void filterActive(){
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted(){
        $(By.linkText("Completed")).click();
    }

    @Step
    public void filterAll(){
        $(By.linkText("All")).click();
    }

}



package com.seren.brn3;

import com.seren.brn3.pages.TodoMVCPage;
import org.junit.Test;

import static com.seren.brn3.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn3.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn3.helpers.GivenHelpers.*;

public class TodoMVCTestAtCompleted extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testCreate(){
        givenAtCompleted(COMPLETED, "A");

        page.add("B");
        page.assertVisibleTasks("A");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEdit(){
        givenAtCompleted(COMPLETED, "A", "B");

        page.edit("B","B_edited");
        page.assertTasks("A","B_edited");
        page.assertItemsLeft(0);
    }

    @Test
    public void testCompleteAll(){
        givenAtCompleted(ACTIVE, "A", "B");

        page.toggleAll();
        page.assertTasks("A","B");
        page.assertItemsLeft(0);
    }

    @Test
    public void testReopenAll(){
        givenAtCompleted(COMPLETED,"A","B");

        page.toggleAll();
        page.assertNoVisibleTasks();
        page.assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtCompleted(new Task("A", ACTIVE), new Task("B",COMPLETED));

        page.cancelEdit("B" , "B_edited and canceled");
        page.assertVisibleTasks("B");
        page.assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtCompleted(COMPLETED, "A", "B");

        page.confirmEditByPressTab("A" , "A_edited");
        page.assertVisibleTasks("A_edited", "B");
        page.assertItemsLeft(0);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtCompleted(COMPLETED, "A", "B");

        page.confirmEditByClickOutside("B" , "B_edited");
        page.assertVisibleTasks("A", "B_edited");
        page.assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtCompleted(COMPLETED, "A","B");

        page.deleteByEmptyingText("B");
        page.assertTasks("A");
        page.assertItemsLeft(0);
    }
}

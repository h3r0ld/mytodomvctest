package com.seren.brn3;


;
import com.seren.brn3.pages.TodoMVCPage;
import org.junit.Test;

import static com.seren.brn3.helpers.GivenHelpers.TaskType.ACTIVE;
import static com.seren.brn3.helpers.GivenHelpers.TaskType.COMPLETED;
import static com.seren.brn3.helpers.GivenHelpers.givenAtActive;
import static com.seren.brn3.helpers.GivenHelpers.givenAtAll;


public class TodoMVCTestAtActive extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testEdit(){
        givenAtActive(ACTIVE, "A");

        page.edit("A","A_edited");
        page.assertTasks("A_edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDelete(){
        givenAtActive(ACTIVE, "A","B");

        page.delete("A");
        page.assertTasks("B");
        page.assertItemsLeft(1);
    }

    @Test
    public void testComplete(){
        givenAtActive(ACTIVE, "A");

        page.toggle("A");
        page.assertNoVisibleTasks();
        page.assertItemsLeft(0);
    }

    @Test
    public void testClearCompleted(){
        givenAtActive(COMPLETED, "A", "B");

        page.clearCompleted();
        page.assertNoTasks();
    }

    @Test
    public void testReopenAll(){
        givenAtActive(COMPLETED, "A","B");

        page.toggleAll();
        page.assertTasks("A","B");
        page.assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtActive(ACTIVE, "A", "B");

        page.cancelEdit("B" , "B_edited and canceled");
        page.assertVisibleTasks("A", "B");
        page.assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtActive(ACTIVE, "A", "B");

        page.confirmEditByPressTab("B" , "B_edited");
        page.assertVisibleTasks("A" , "B_edited");
        page.assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtActive(ACTIVE, "A");

        page.confirmEditByClickOutside("A" , "A_edited");
        page.assertVisibleTasks("A_edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtActive(ACTIVE,"A", "B");

        page.deleteByEmptyingText("A");
        page.assertTasks("B");
        page.assertItemsLeft(1);
    }
}

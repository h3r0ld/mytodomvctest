package com.seren.brn3;

import com.seren.brn3.pages.TodoMVCPage;
import org.junit.Test;

import static com.seren.brn3.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn3.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn3.helpers.GivenHelpers.*;

public class TodoMVCTestAtAll extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testDelete(){
        givenAtAll(ACTIVE,"A");

        page.delete("A");
        page.assertNoTasks();
    }

    @Test
    public void testCompleteAll(){
        givenAtAll(ACTIVE, "A","B");

        page.toggleAll();
        page.assertTasks("A","B");
        page.assertItemsLeft(0);
    }

    @Test
    public void testClearCompleted(){
        givenAtAll(new Task("A", ACTIVE), new Task("B", COMPLETED),new Task("C",COMPLETED));

        page.clearCompleted();
        page.assertTasks("A");
        page.assertItemsLeft(1);
    }

    @Test
    public void testReopen(){
        givenAtAll(COMPLETED, "A");

        page.toggle("A");
        page.assertTasks("A");
        page.assertItemsLeft(1);
    }

    @Test
    public void testReopenAll(){
        givenAtAll(COMPLETED, "A","B");

        page.toggleAll();
        page.assertTasks("A","B");
        page.assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtAll(ACTIVE, "A");

        page.cancelEdit("A" , "A_edited and canceled");
        page.assertVisibleTasks("A");
        page.assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtAll(ACTIVE, "A");

        page.confirmEditByPressTab("A" , "A_edited");
        page.assertVisibleTasks("A_edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtAll(ACTIVE, "A", "B");

        page.confirmEditByClickOutside("A" , "A_edited");
        page.assertVisibleTasks("A_edited", "B");
        page.assertItemsLeft(2);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtAll(ACTIVE, "A");

        page.deleteByEmptyingText("A");
        page.assertNoTasks();
    }
}

package com.seren.brn3;

import com.seren.brn3.pages.TodoMVCPage;
import org.junit.Test;

import static com.seren.brn3.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn3.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn3.helpers.GivenHelpers.*;

public class TodoMVCTestGeneral extends BaseTest{

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testTasksCommonFlow(){
        givenAtAll();
        page.add("A");
        page.edit("A" , "A_edited");

        //complete
        page.toggle("A_edited");
        page.assertTasks("A_edited");

        page.filterActive();
        page.assertNoVisibleTasks();

        page.add("B");
        page.assertVisibleTasks("B");
        page.assertItemsLeft(1);

        //complete all
        page.toggleAll();
        page.assertNoVisibleTasks();

        page.filterCompleted();
        page.assertVisibleTasks("A_edited","B");

        //reopen
        page.toggle("A_edited");
        page.assertVisibleTasks("B");

        page.clearCompleted();
        page.assertNoVisibleTasks();

        page.filterAll();
        page.assertTasks("A_edited");

        page.delete("A_edited");
        page.assertNoTasks();
    }

    @Test
    public void testSwitchingFilterAllToCompleted(){
        givenAtAll(new Task("A",COMPLETED),  new Task("B",ACTIVE), new Task("C", COMPLETED));

        page.filterCompleted();
        page.assertVisibleTasks("A", "C");
        page.assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterActiveToAll(){
        givenAtActive(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        page.filterAll();
        page.assertTasks("A","B");
        page.assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterCompletedToActive(){
        givenAtCompleted(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        page.filterActive();
        page.assertVisibleTasks("A");
        page.assertItemsLeft(1);
    }
}

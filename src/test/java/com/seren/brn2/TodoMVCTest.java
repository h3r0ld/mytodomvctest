package com.seren.brn2;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;


import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.seren.brn2.helpers.GivenHelpers.TaskType.COMPLETED;
import static com.seren.brn2.helpers.GivenHelpers.TaskType.ACTIVE;
import static com.seren.brn2.helpers.GivenHelpers.givenAtActive;
import static com.seren.brn2.helpers.GivenHelpers.givenAtCompleted;
import static com.seren.brn2.helpers.GivenHelpers.*;

public class TodoMVCTest {

    @Test
    public void testTasksCommonFlow(){
        givenAtAll();
        add("A");
        edit("A" , "A_edited");

        //complete
        toggle("A_edited");
        assertTasks("A_edited");

        filterActive();
        assertNoVisibleTasks();

        add("B");
        assertVisibleTasks("B");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A_edited","B");

        //reopen
        toggle("A_edited");
        assertVisibleTasks("B");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertTasks("A_edited");

        delete("A_edited");
        assertNoTasks();
    }

    @Test
    public void testCreateAtCompleted(){
        givenAtCompleted(COMPLETED, "A");

        add("B");
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtActive(){
        givenAtActive(ACTIVE, "A");

        edit("A","A_edited");
        assertTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtCompleted(){
        givenAtCompleted(COMPLETED, "A", "B");

        edit("B","B_edited");
        assertTasks("A","B_edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteAtAll(){
        givenAtAll(ACTIVE,"A");

        delete("A");
        assertNoTasks();
    }

    @Test
    public void testDeleteAtActive(){
        givenAtActive(ACTIVE, "A","B");

        delete("A");
        assertTasks("B");
        assertItemsLeft(1);
    }


    @Test
    public void testCompleteAtActive(){
        givenAtActive(ACTIVE, "A");

        toggle("A");
        assertNoVisibleTasks();
        assertItemsLeft(0);
    }


    @Test
    public void testCompleteAllAtAll(){
        givenAtAll(ACTIVE, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAllAtCompleted(){
        givenAtCompleted(ACTIVE, "A", "B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAll(){
        givenAtAll(new Task("A", ACTIVE), new Task("B", COMPLETED),new Task("C",COMPLETED));

        clearCompleted();
        assertTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedAtActive(){
        givenAtActive(COMPLETED, "A", "B");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testReopenAtAll(){
        givenAtAll(COMPLETED, "A");

        toggle("A");
        assertTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtAll(){
        givenAtAll(COMPLETED, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(2);
    }

    @Test
    public void testReopenAllAtActive(){
        givenAtActive(COMPLETED, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(2);
    }

    @Test
    public void testReopenAllAtCompleted(){
        givenAtCompleted(COMPLETED,"A","B");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEditAtAll(){
        givenAtAll(ACTIVE, "A");

        cancelEdit("A" , "A_edited and canceled");
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtActive(){
        givenAtActive(ACTIVE, "A", "B");

        cancelEdit("B" , "B_edited and canceled");
        assertVisibleTasks("A", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEditAtCompleted(){
        givenAtCompleted(new Task("A", ACTIVE), new Task("B",COMPLETED));

        cancelEdit("B" , "B_edited and canceled");
        assertVisibleTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTabAtAll(){
        givenAtAll(ACTIVE, "A");

        confirmEditByPressTab("A" , "A_edited");
        assertVisibleTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTabAtActive(){
        givenAtActive(ACTIVE, "A", "B");

        confirmEditByPressTab("B" , "B_edited");
        assertVisibleTasks("A" , "B_edited");
        assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByPressTabAtCompleted(){
        givenAtCompleted(COMPLETED, "A", "B");

        confirmEditByPressTab("A" , "A_edited");
        assertVisibleTasks("A_edited", "B");
        assertItemsLeft(0);
    }

    @Test
    public void testConfirmEditByClickAtAll(){
        givenAtAll(ACTIVE, "A", "B");

        confirmEditByClickOutside("A" , "A_edited");
        assertVisibleTasks("A_edited", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByClickAtActive(){
        givenAtActive(ACTIVE, "A");

        confirmEditByClickOutside("A" , "A_edited");
        assertVisibleTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByClickAtCompleted(){
        givenAtCompleted(COMPLETED, "A", "B");

        confirmEditByClickOutside("B" , "B_edited");
        assertVisibleTasks("A", "B_edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptyingAtAll(){
        givenAtAll(ACTIVE, "A");

        deleteByEmptyingText("A");
        assertNoTasks();
    }

    @Test
    public void testDeleteByEmptyingAtActive(){
        givenAtActive(ACTIVE,"A", "B");

        deleteByEmptyingText("A");
        assertTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingAtCompleted(){
        givenAtCompleted(COMPLETED, "A","B");

        deleteByEmptyingText("B");
        assertTasks("A");
        assertItemsLeft(0);
    }

    @Test
    public void testSwitchingFilterAllToCompleted(){
        givenAtAll(new Task("A",COMPLETED),  new Task("B",ACTIVE), new Task("C", COMPLETED));

        filterCompleted();
        assertVisibleTasks("A", "C");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterActiveToAll(){
        givenAtActive(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        filterAll();
        assertTasks("A","B");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterCompletedToActive(){
        givenAtCompleted(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        filterActive();
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }

    ElementsCollection tasks = $$("#todo-list>li");
    SelenideElement newTodo = $("#new-todo");


    @Step
    public void add(String... taskTexts) {
        for(String text : taskTexts){
            newTodo.setValue(text).pressEnter();
        }
    }

    @Step
    public void assertTasks(String... taskTexts){
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertVisibleTasks(String... taskTexts){
        tasks.filterBy(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertNoVisibleTasks(){
        tasks.filterBy(visible).shouldBe(empty);
    }

    @Step
    public void assertItemsLeft(int tasksLeft){
        $("#todo-count>strong").shouldBe(exactText(Integer.toString(tasksLeft)));
    }

    @Step
    public void delete(String taskText){
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public void toggle(String taskText){
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public void clearCompleted(){
        $("#clear-completed").click();
    }

    @Step
    public void toggleAll(){
        $("#toggle-all").click();
    }

    @Step
    public void assertNoTasks(){
        tasks.shouldBe(empty);
    }

    @Step
    public void edit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public void cancelEdit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public void confirmEditByPressTab(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    @Step
    public void confirmEditByClickOutside(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    @Step
    public void deleteByEmptyingText(String TaskText){
        startEdit(TaskText, "").pressEnter();
    }

    @Step
    public SelenideElement startEdit (String oldTaskText, String newTaskText){
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public void filterActive(){
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted(){
        $(By.linkText("Completed")).click();
    }

    @Step
    public void filterAll(){
        $(By.linkText("All")).click();
    }

}

package com.seren.brn4;

import org.junit.Test;

import static com.seren.brn4.helpers.GivenHelpers.TaskType.ACTIVE;
import static com.seren.brn4.helpers.GivenHelpers.TaskType.COMPLETED;
import static com.seren.brn4.helpers.GivenHelpers.givenAtActive;
import static com.seren.brn4.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn4.pages.TodoMVC.*;


public class TodoMVCTestAtActive extends BaseTest {

    @Test
    public void testEdit(){
        givenAtActive(ACTIVE, "A");

        edit("A","A_edited");
        assertTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDelete(){
        givenAtActive(ACTIVE, "A","B");

        delete("A");
        assertTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testComplete(){
        givenAtActive(ACTIVE, "A");

        toggle("A");
        assertNoVisibleTasks();
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompleted(){
        givenAtActive(COMPLETED, "A", "B");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testReopenAll(){
        givenAtActive(COMPLETED, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtActive(ACTIVE, "A", "B");

        cancelEdit("B" , "B_edited and canceled");
        assertVisibleTasks("A", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtActive(ACTIVE, "A", "B");

        confirmEditByPressTab("B" , "B_edited");
        assertVisibleTasks("A" , "B_edited");
        assertItemsLeft(2);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtActive(ACTIVE, "A");

        confirmEditByClickOutside("A" , "A_edited");
        assertVisibleTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtActive(ACTIVE,"A", "B");

        deleteByEmptyingText("A");
        assertTasks("B");
        assertItemsLeft(1);
    }
}

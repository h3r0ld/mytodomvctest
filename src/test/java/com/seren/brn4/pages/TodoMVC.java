package com.seren.brn4.pages;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class TodoMVC {
    static ElementsCollection tasks = $$("#todo-list>li");
    static SelenideElement newTodo = $("#new-todo");


    @Step
    public static void add(String... taskTexts) {
        for(String text : taskTexts){
            newTodo.setValue(text).pressEnter();
        }
    }

    @Step
    public static void assertTasks(String... taskTexts){
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public static void assertVisibleTasks(String... taskTexts){
        tasks.filterBy(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public static void assertNoVisibleTasks(){
        tasks.filterBy(visible).shouldBe(empty);
    }

    @Step
    public static void assertItemsLeft(int tasksLeft){
        $("#todo-count>strong").shouldBe(exactText(Integer.toString(tasksLeft)));
    }

    @Step
    public static void delete(String taskText){
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public static void toggle(String taskText){
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    @Step
    public static void clearCompleted(){
        $("#clear-completed").click();
    }

    @Step
    public static void toggleAll(){
        $("#toggle-all").click();
    }

    @Step
    public static void assertNoTasks(){
        tasks.shouldBe(empty);
    }

    @Step
    public static void edit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public static void cancelEdit(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public static void confirmEditByPressTab(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    @Step
    public static void confirmEditByClickOutside(String oldTaskText, String newTaskText){
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    @Step
    public static void deleteByEmptyingText(String TaskText){
        startEdit(TaskText, "").pressEnter();
    }

    @Step
    public static SelenideElement startEdit (String oldTaskText, String newTaskText){
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public static void filterActive(){
        $(By.linkText("Active")).click();
    }

    @Step
    public static void filterCompleted(){
        $(By.linkText("Completed")).click();
    }

    @Step
    public static void filterAll(){
        $(By.linkText("All")).click();
    }

}

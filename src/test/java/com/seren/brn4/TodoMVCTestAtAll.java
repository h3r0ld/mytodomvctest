package com.seren.brn4;

import org.junit.Test;

import static com.seren.brn4.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn4.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn4.helpers.GivenHelpers.*;
import static com.seren.brn4.pages.TodoMVC.*;

public class TodoMVCTestAtAll extends BaseTest {   

    @Test
    public void testDelete(){
        givenAtAll(ACTIVE,"A");

        delete("A");
        assertNoTasks();
    }

    @Test
    public void testCompleteAll(){
        givenAtAll(ACTIVE, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompleted(){
        givenAtAll(new Task("A", ACTIVE), new Task("B", COMPLETED),new Task("C",COMPLETED));

        clearCompleted();
        assertTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testReopen(){
        givenAtAll(COMPLETED, "A");

        toggle("A");
        assertTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAll(){
        givenAtAll(COMPLETED, "A","B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtAll(ACTIVE, "A");

        cancelEdit("A" , "A_edited and canceled");
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtAll(ACTIVE, "A");

        confirmEditByPressTab("A" , "A_edited");
        assertVisibleTasks("A_edited");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtAll(ACTIVE, "A", "B");

        confirmEditByClickOutside("A" , "A_edited");
        assertVisibleTasks("A_edited", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtAll(ACTIVE, "A");

        deleteByEmptyingText("A");
        assertNoTasks();
    }
}
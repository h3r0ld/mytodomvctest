package com.seren.brn4;

import org.junit.Test;

import static com.seren.brn4.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn4.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn4.helpers.GivenHelpers.*;
import static com.seren.brn4.pages.TodoMVC.*;

public class TodoMVCTestGeneral extends BaseTest{
    
    @Test
    public void testTasksCommonFlow(){
        givenAtAll();
        add("A");
        edit("A" , "A_edited");

        //complete
        toggle("A_edited");
        assertTasks("A_edited");

        filterActive();
        assertNoVisibleTasks();

        add("B");
        assertVisibleTasks("B");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A_edited","B");

        //reopen
        toggle("A_edited");
        assertVisibleTasks("B");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertTasks("A_edited");

        delete("A_edited");
        assertNoTasks();
    }

    @Test
    public void testSwitchingFilterAllToCompleted(){
        givenAtAll(new Task("A",COMPLETED),  new Task("B",ACTIVE), new Task("C", COMPLETED));

        filterCompleted();
        assertVisibleTasks("A", "C");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterActiveToAll(){
        givenAtActive(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        filterAll();
        assertTasks("A","B");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchingFilterCompletedToActive(){
        givenAtCompleted(new Task("A",ACTIVE),  new Task("B",COMPLETED));

        filterActive();
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }
}
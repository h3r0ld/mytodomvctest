package com.seren.brn4;

import org.junit.Test;

import static com.seren.brn4.helpers.GivenHelpers.givenAtAll;
import static com.seren.brn4.helpers.GivenHelpers.TaskType.*;
import static com.seren.brn4.helpers.GivenHelpers.*;
import static com.seren.brn4.pages.TodoMVC.*;

public class TodoMVCTestAtCompleted extends BaseTest {

    @Test
    public void testCreate(){
        givenAtCompleted(COMPLETED, "A");

        add("B");
        assertVisibleTasks("A");
        assertItemsLeft(1);
    }

    @Test
    public void testEdit(){
        givenAtCompleted(COMPLETED, "A", "B");

        edit("B","B_edited");
        assertTasks("A","B_edited");
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAll(){
        givenAtCompleted(ACTIVE, "A", "B");

        toggleAll();
        assertTasks("A","B");
        assertItemsLeft(0);
    }

    @Test
    public void testReopenAll(){
        givenAtCompleted(COMPLETED,"A","B");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit(){
        givenAtCompleted(new Task("A", ACTIVE), new Task("B",COMPLETED));

        cancelEdit("B" , "B_edited and canceled");
        assertVisibleTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTab(){
        givenAtCompleted(COMPLETED, "A", "B");

        confirmEditByPressTab("A" , "A_edited");
        assertVisibleTasks("A_edited", "B");
        assertItemsLeft(0);
    }

    @Test
    public void testConfirmEditByClick(){
        givenAtCompleted(COMPLETED, "A", "B");

        confirmEditByClickOutside("B" , "B_edited");
        assertVisibleTasks("A", "B_edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteByEmptying(){
        givenAtCompleted(COMPLETED, "A","B");

        deleteByEmptyingText("B");
        assertTasks("A");
        assertItemsLeft(0);
    }
}
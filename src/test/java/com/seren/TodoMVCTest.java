package com.seren;


import org.junit.Test;



import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

public class TodoMVCTest {


    @Test
    public void testCommonFlow(){

        open("https://todomvc4tasj.herokuapp.com/");

        //Additions of tasks
        $("#new-todo").setValue("task1").pressEnter();
        $("#new-todo").setValue("task2").pressEnter();
        $("#new-todo").setValue("task3").pressEnter();
        $("#new-todo").setValue("task4").pressEnter();
        $$("#todo-list li").shouldHave(exactTexts("task1","task2","task3","task4"));

        //Deletion of tasks2
        $("#todo-list li:nth-of-type(2)").hover();
        $("#todo-list li:nth-of-type(2) .destroy").click();
        $$("#todo-list li").shouldHave(exactTexts("task1","task3","task4"));

        //mark task4 as completed
        $("#todo-list li:nth-of-type(3)  .toggle").click();
        $$("#todo-list li").shouldHave(exactTexts("task1","task3","task4"));

        //clear completed
        $("#clear-completed").click();
        $$("#todo-list li").shouldHave(exactTexts("task1","task3"));

        //mark all as completed, clear completed
        $("#toggle-all").click();
        $$("#todo-list li").shouldHave(exactTexts("task1","task3"));
        $("#clear-completed").click();
        $$("#todo-list li").shouldBe(empty);
    }
}
